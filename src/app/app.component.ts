import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  myForm: FormGroup;
  primeList = [];
  isValid = true;
  errorText: string;
  constructor(private _fb: FormBuilder) { } // form builder simplify form initialization

  ngOnInit() {
    this.myForm = this._fb.group({
      lower: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
      upper: ['', [<any>Validators.required, <any>Validators.minLength(5)]]
    });
  }

  onSubmit() {
    this.primeList = [];
    this.isValid = true;
    let lower = this.myForm.value.lower;
    let upper = this.myForm.value.upper;
    if (isNaN(lower) || isNaN(upper)) {
      this.isValid = false;
      this.errorText = "Value must be number"
    } else if (lower < 0 || upper > 10000) {
      this.isValid = false;
      this.errorText = "Value must be between 0 to 10000"
    }else if (lower >= upper) {
      this.isValid = false;
      this.errorText = "Lower bound must be less than the upper bound"
    } else {
      for (let counter = lower; counter <= upper; counter++) {
        let notPrime = false;
        for (let i = 2; i <= counter; i++) {
          if (counter%i===0 && i!==counter) {
            notPrime = true;
          }
        }
        if (notPrime === false) {
          this.primeList.push(counter);
        }
      }
    }


  }

}
