import { PrimenatorPage } from './app.po';

describe('primenator App', function() {
  let page: PrimenatorPage;

  beforeEach(() => {
    page = new PrimenatorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
